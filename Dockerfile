FROM golang:latest
WORKDIR /root
ADD https://gitlab.com/scynami/stepacadproj/-/raw/master/main.go .
RUN go build main.go

FROM alpine:latest  
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /root/main .
CMD ["./main"]  